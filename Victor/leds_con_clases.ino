/*Este pequeño programa prende 3 leds (uno de cada color, uno a la vez)
 *con la peculiaridad de usar objetos.
 */

//Declaro una clase con el nombre Led
class Led{  
private:  			
  int pinDigital;	/*Variable que almacena el número del pin digital.
  					 *Esta variable debería ser del tipo byte
                     *pero para un mejor entendimiento del código
                     *elegí hacerla del tipo int.
                     */
  int tiempo;		//Variable que almacena el tiempo de espera.  					

public:  
  /*Esta es una función especial que se la conoce como Constructor.
   *Debe ser publica y llevar el mismo nombre que la clase
   *Si no existe el compilador la crea automaticamente.
   *Puede usarse para asignar valores a variables y lanzar funciones.
   *En este caso esta vacía.
   */
  Led(){			//Constructor vacío.
  }
  
  /*Función que recibe el número del pin a activar
   *y el tiempo deseado de espera para delay().
   *luego lo asigna a las variables correspondientes.
   */
  void setup(int pinDigital, int tiempo)
  {
    this->pinDigital=pinDigital; //Se asignan los valores recibidos desde la función
    this->tiempo=tiempo;
    pinMode(pinDigital, OUTPUT); //Se configura el pin recibido como salida
  }
  
  /*Función que apaga y prende el led.
   *Se la llama desde la función loop del bucle principal.
   *Esta función se puede mejorar para evitar el bloqueo en tiempo
   *de ejecución (delay) haciendo una medición del delta del tiempo(dt).
   */
  void loop(){
     digitalWrite(pinDigital, HIGH);	//Encendemos el led
     delay(tiempo);						//Esperamos 
     digitalWrite(pinDigital, LOW);     //Apagamos el led
 	 delay(tiempo);       
    
  }  
};

/*Instancio 3 objetos de la clase Led
 *los llamo rojo, amarillo y verde.
 */
Led verde; 
Led rojo; 
Led amarillo;

//Trabajo en las funciones de Arduino como lo hago habitualmente
void setup() {
 verde.setup(8,1000); 	//Llamo a la función setup implementada en Led pasandole pin y tiempo
 amarillo.setup(9,750);
 rojo.setup(10,500);  
}

void loop() {
  verde.loop(); 	//Llamo a la función loop implementada dentro de la clase Led
  amarillo.loop();	
  rojo.loop();	
}
