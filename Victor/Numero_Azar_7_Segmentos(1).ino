/*Este pequeño fragmento de código genera un numero aleatorio y lo muestra 
en el display de 7 segmentos, hace uso del sistema de numeración binario,
se puede hacer en hexa o en combinación con ambos.
Además se aplican operaciones a nivel de bits.
Hay que tener en cuenta el orden de los cables, según como este el cableado
pueden cambiar ciertos argumentos (como HIGH o LOW) y valores del binario.
*/
char codigosDisplay[10] = { B0111111, B0000110,B1011011, B1001111,B1100110,
                            B1101101, B1111101, B0000111, B1111111,B1100111}; /*Este arreglo
                            contiene a los nueve números (0 a 9) que se pueden mostrar en el display,
                            estan en binario, también se puede representar en hexadecimal*/

int pin[7] = {6,7,8,9,10,11,12}; /*Pines de salida */

void setup()
{  
  /*Inicializo los pines de definidos en el arreglo "pin" como salida*/
  for (int i=0; i<7; i++)
  {
    pinMode(pin[i],OUTPUT);
  }
  /*Inicio una semilla que me permitirá generar un número pseudo-aleatorio*/
   randomSeed(analogRead(0));      
}

void loop() 
{
  limpiaLeds();/*Función que apaga los leds*/
  numeros();  /*Función que seleciona un numero al azar y lo muestra en el display*/
  delay(4000); /*Un delay para frenar la ejecución del codigo*/
}

/*Función que genera un numero pseudo aleatorio y lo dibuja en el display*/
void numeros(){
  char n = codigosDisplay[random(0, 10)]; /* La variable n va a tomar cualquiera de los valores definidos 
  en codigosDisplay, que son los números del 0 al 9*/
  int mascara = 1; /*Esta variable toma el valor 1 que es igual a 00000001 en binario, nos va a servir para 
  comparar bits */
  for(int i=0; i<7; i++){  /*Como tenemos 7 pines que activar o desactivar creamos un bucle acorde*/
      if((n & mascara) == 0){ /*Comparamos el último bit de mascara, con el último bit de n*/
         digitalWrite(pin[i], HIGH); /*Según el caso encendemos o apagamos el led del display*/
      }else{
         digitalWrite(pin[i], LOW);
      }  
      mascara = mascara << 1; /*Desplazamos el 1 un bit a la izquierda*/      
  }  
}

/*Funcion que apaga los leds del display*/
void limpiaLeds(){ 
  for (int i=0; i<7; i++)
  {
    digitalWrite(pin[i],HIGH);
  } 
}
  
