/*El siguiente código usa la instrucción ldi para 
 *cargar un valor de 8 bit (1 byte) en el registro indicado.
 *En este caso es el registro 24 donde cargamos el valor 121
 *Después hacemos uso de la instrucción sts la cual
 *permite poner los contenidos del registro 24 en la dirección de 
 *memoria de la variable a.
 *Finalmente imprimimos el valor de la variable a vía monitor serie.
 */

volatile byte a=0;

void setup()
{
  Serial.begin(9600);
   asm (
    "ldi r24, 121  \n" //cargo el registro r26 con el valor 42
    "sts (a), r24 \n" //almaceno r26 en la ubicacion de memoria de la variable a
  );  
  Serial.print("el valor de a es "); Serial.println(a);  
}

void loop()
{
 
}

/*Referencias a las instrucciones usadas
 *sts - https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_STS.html
 *ldi - https://www.microchip.com/webdoc/avrassembler/avrassembler.wb_LDI.html
 */