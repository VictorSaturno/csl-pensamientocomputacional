/*Ejemplo muy simple del uso de la instrucción
 *nop, las misma interrumpe la ejecución del código
 *por 62.5ns (equivale a un ciclo)
 *también se puede hacer algo así:
 * asm ( "nop \n nop \n nop \n nop \n nop \n nop \n" );  
 *para comprobar el funcionamiento de la instrucción
 */

unsigned long tiempo;

void setup() {
  Serial.begin(9600);
}
void loop() {
  Serial.print("Tiempo: ");
  tiempo = millis();
  asm ( "nop \n" );  
  Serial.println(tiempo); 
}