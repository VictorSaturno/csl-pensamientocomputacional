
/*Este programa hace encender un display de 7 segmentos (7 leds, por ejemplo), para cada 
 *uno de los dígitos decimales, desde el 0 al 9.
 *Es para un solo dígito . Para más dígitos se utilizarán pines adicionales, 
 *que habiliten el terminal común (masa), de cada display.
 *Utiliza una variable de 1 byte llamada "Num" donde se almacena el 
 *código de 7 segmentos del número a exhibir.
 *Esta palabra de 1 byte se lee bit por bit y el estado de ese bit se
 *escribe en el pin correspondiente que se conecta a cada segmento, hasta completar
 *el símbolo.
 *Los bits en la variable Num, se leen desde la derecha (<<< en este sentido<<<<) 
 *Por eso hay que conectar el segmento (a) al bit menos significativo.
 *
 *El programa utiliza la puerta de comunicaciones para mostrar cada dígito que se va a exhibir.
 *Además muestra el código de los 7 segmentos (en decimal, hexadecimal y binario), 
 *el segmento a encender y el estado que toma el pin asociado a ese segmento.
 *
 *Es un buen ejercicio para practicar el uso de arreglos, la sentencia FOR, la impresión y
 *los sistemas de numeración. 
 *Cualquier duda, consulte a su médico.
 */

/********************************************/
byte Num = 0;
byte i = 0;
char Segmentos[]= {'a', 'b', 'c', 'd', 'e', 'f', 'g'};/* c/u de los 7 seg del display*/

/* dígitos[]= {0, 1, 2, 3, 4, 5, 6, 7, 8, 9} ; 
*  son los caracteres a mostrar (no uso este array,es sólo para que se vea el 
   origen de los símbolos, pero bien se podría usar en otro método para encender los leds */
   
//Array con los códigos decimales p/encender display 7 segmentos:
byte  Displ[]={63, 6, 91, 79, 102,  109, 125, 7, 127, 111}; 
/*Ej: 63= 3F(hexadecimal) = 111111 (binario)representa el cero. 6 representa el 1, etc*/
byte pin =2;
boolean  Estado=0;
byte bitSegm =0;  //Orden del bit a leer
/****************/
void setup() 
{
  Serial.begin(9600); /*Preparo la puerta de comunicaciones*/
  for(pin = 2; pin < 9; pin++) 
  /*utilizaré 7 pines (desde el 2 al 9) para encender los segmentos*/
  {
  pinMode(pin, OUTPUT);
  }

}
void loop()
{
   for (i = 0; i < 10; i++) /*los 10 dígitos decimales*/
   {

    Num = Displ[i]; /*tomo el código de 7 segmentos para el dígito decimal elegido*/
  
    /*Imprimo en pantalla, para asegurarme que no haya errores, antes de armar el circuito*/
    /*Y porque no tengo el display :) */
    Serial.println("EN ESTE CURSO SE APRENDE MUCHO");
    Serial.print("Nº DECIMAL A MOSTRAR:         ");
    Serial.println(i);
    Serial.print("Nº decimal del byte que guarda los 7 segmentos  ");
    Serial.println(Num);
    Serial.print("Nº hexadecimal del codigo de 7  Segmentos:      ");
    Serial.println(Num, HEX);
    Serial.print("binario del codigo de 7  Segmentos:        ");
    Serial.println(Num, BIN);
    
    Mostrar () ;  /*función que vuelca los bits para encender los segmentos*/
    delay (20000); /*Espera, para no llenar la pantalla de números ni saturar el buffer*/
                  /*O para tomar un mate*/
   }
}//fin del for
/* */
  void Mostrar () //función que maneja el display de 7 segmentos
  {
 //La instrucción FOR inicializa 2 variables en la misma expresión, también las incrementa. 
   
   for((bitSegm=0,pin=2); bitSegm<7;(bitSegm++,pin++))/* 0=a, 1=b, 2=c, 3=d, 4=e, 5=f, 6=g */
      {
        Estado = bitRead(Num, bitSegm);   /*Num es la cifra a mostrar*/
                                       /*bitSegm es el bit asociado al segmento */
        digitalWrite(pin, Estado); /*pin es el Nº del pin de salida al segmento (del 2 al 8)*/
        Serial.print("SEGMENTO: ");
        Serial.println(Segmentos[bitSegm]);
        Serial.print("Orden del bit en el byte(0=LSB): ");
        Serial.println(bitSegm);
        Serial.print("pin correspondiente: Nº ");
        Serial.println(pin);
        Serial.print("Estado del pin: ");
        Serial.println(Estado);
      } 
      
  } //fin de mostrar()
