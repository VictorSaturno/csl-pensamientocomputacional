/* Este programa enciende alternativamente 2 Leds: 
(el interno de la placa y otro led externo), 
cada 1 segundo */
/* Para no colocar retardos, (Delays) que desperdician los recursos
 *  del sistema, utiliza el reloj interno del microcontrolador,
 *  y mide la diferencia entre la última medición 
 *  en que se encendió un led y la lectura actual del reloj. 
 *  Cuando dicha diferencia es igual o supera 1000 milisegundos, 
   cambio el estado de los leds. 
   Si la cantidad de comentarios parece excesiva, 
   aclaro que no siempre será así.   */
   
   /*-------------------------------*/
 
int led = 13;       /* El pin 13 de la placa, o led interno*/
int led12 = 12;     /* El pin 12 de la placa, o led externo*/
int Intervalo;      /* Tiempo entre parpadeos*/
long Lectura_anterior; /* Lectura anterior del reloj interno*/

void setup()
  {
    
    pinMode(led,OUTPUT);
    pinMode(led12,OUTPUT);
    Serial.begin(9600); /*Preparo la puerta de comunicaciones*/
  }
void loop()
{
 unsigned long Tiempo_Actual = millis();/* Reloj interno */

  boolean Luz;   /*Indica si debe estar encendido el led
                   de la placa (y apagado el externo) */
 
if (abs(Tiempo_Actual - Lectura_anterior) >= 1000) /* Comparo contra 1 segundo */
  {
  Intervalo = (Tiempo_Actual - Lectura_anterior); /* Pongo este indicador para visualizarlo*/

  Lectura_anterior = Tiempo_Actual; /* Reseteo el contador de milisegundos cuando 
                                    llega a 1000 */
  
 
    if (Luz == true)
   {
   
   digitalWrite(led,HIGH);  /*enciendo led placa*/
   digitalWrite(led12,LOW); // apago led del pin 12
   
   
    Luz = false;     /* indica que el led de la placa 
                      debe apagarse en el próximo ciclo*/
   }
    else
      {
   
   digitalWrite(led,LOW);     /*apago led placa */
   digitalWrite(led12,HIGH);  /*enciendo led del pin 12 */
   
  
    

   Serial.print("Tiempo: "); /* Tiempo no es una variable sino un texto
                               a imprimir en pantalla */ 

/* presento en pantalla los indicadores */

    Serial.println(Tiempo_Actual);  
    Serial.print("Intervalo:  ");
    Serial.println(Intervalo);
    Serial.print("Luz:  ");
    Serial.println(Luz);
    
    Luz = true;       /* indica que el led de la placa debe encenderse
                        en el próximo ciclo */
      }             /* fin del else*/

 }                  /* fin del primer if*/

 
}                       /* vuelve al comienzo del loop */
